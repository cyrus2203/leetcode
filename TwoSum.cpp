// The MIT License (MIT)

// Copyright (c) YEAR NAME

//  Permission is hereby granted, free of charge, to any person obtaining a
//  copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.

/* code */

#include "TwoSum.h"
#include <iostream>
#include <thread>

TwoSum::TwoSum(/* args */)
{
}

TwoSum::~TwoSum()
{
}

std::vector<int> TwoSum::twoSum(std::vector<int>& nums, int target){
    for(std::vector<int>::iterator it = nums.begin(); it != nums.end(); ++it) {
        // check if complement of the current value already exists in the Map
        // If not, add it top the Map
        if((mHash.find(target-*it) != mHash.end())) {
            mResult.push_back(std::distance(nums.begin(),it));
            mResult.push_back(mHash[target-*it]);
            return mResult;    
        } else {
            mHash.insert(std::make_pair(*it, std::distance(nums.begin(),it)));
        }
    }
    return mResult;
}