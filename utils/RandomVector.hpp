#include <vector>
#include <cstdlib>
#include <algorithm>
#include <iostream>

template<typename T>
class RandomVector
{
private:
    std::vector<T> mRandVect;
public:
    RandomVector(int size);
    ~RandomVector();
    std::vector<T>& get();
};

template<typename T>
RandomVector<T>::RandomVector(int size) {
    // reserve size of this vector

    // reserve() only changes the capacity and not the size
    //mRandVect.reserve(size);
    mRandVect.resize(size);
}

template<typename T>
RandomVector<T>::~RandomVector() {
}

template<typename T>
std::vector<T>& RandomVector<T>::get() {
    std::generate(mRandVect.begin(), mRandVect.end(), std::rand);
    return mRandVect;
}