// The MIT License (MIT)

// Copyright (c) 2019

//  Permission is hereby granted, free of charge, to any person obtaining a
//  copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.

/* code */

#include <iostream>

#include "TwoSum.h"

#include "utils/Time.h"
#include "utils/RandomVector.hpp"

enum {
    TWOSUM,
};

int main(int argc, char const *argv[])
{
    {
        Time measure;
        TwoSum tSum;

        std::vector<int> iVector = RandomVector<int>(1000000).get(); 
        std::cout << "Input: ";
        std::cout << iVector[500] << " " << iVector[9999] << std::endl;
        auto outVector = tSum.twoSum(iVector, iVector[500] + iVector[9999]);

        //Output
        std::cout << "Output: ";
        for (auto &i : outVector) {
            std::cout << i << ":" << iVector[i] << " "; 
        }
        std::cout << std::endl;
    }
    return 0;
}
