// The MIT License (MIT)

// Copyright (c) YEAR NAME

//  Permission is hereby granted, free of charge, to any person obtaining a
//  copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.

/* code */

#include <vector>
#include <unordered_map>

/*
Problem Statement:
Given an array of integers, return indices of the two numbers such that they add up to a specific target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
Example:
    Given nums = [2, 7, 11, 15], target = 9,

    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].

Simple Approach
    The obvious way would be the brute force approach.
    which will result in O(n^2) execution time. Space
    Complexity is O(n)

Best current approach:
    Runtime Complexity O(n), if HashMap find() is amortized to O(1)
    Space Complexity O(2n)

    While looping through elements of the vector, we can
    store the element and its index to a hash map. In the
    process, we can also check if complement of the current
    element exists. If it exists, we can return index of
    the current element as well as the index of the complement
    which is stored as a Value of the hashMap (Key, Value)

What else did you learn?
    unordered_map
*/
class TwoSum
{
private:
    /* data */
    std::unordered_map<int, int> mHash;
    std::vector<int> mResult; 
public:
    TwoSum(/* args */);
    ~TwoSum();
    std::vector<int> twoSum(std::vector<int>& nums, int target);
};